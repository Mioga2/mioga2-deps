%define _topdir			/root/rpmbuild/
%define _tmppath		%{_topdir}/tmp
%define _prefix			/usr/local
%define _defaultdocdir		%{_prefix}/share/doc
%define _mandir			%{_prefix}/share/man

%define name			mioga2-deps
%define summary			Dependencies for Mioga2
%define version			2.4.26
%define release			5
%define license			GPL
%define group			Documentation
%define vendor			Alixen Labs.
%define packager		Alixen Labs.
%define	buildroot		%{_tmppath}/%{name}-root

Name:      %{name}
Version:   %{version}
Release:   %{release}
Packager:  %{packager}
Vendor:    %{vendor}
License:   %{license}
Summary:   %{summary}
Group:     %{group}
#Source:    %{source}
Prefix:    %{_prefix}
Buildroot: %{buildroot}
BuildArch: noarch
Requires:  perl httpd libxslt libxslt-devel postgresql-server perl-Crypt-SSLeay perl-Date-Calc perl-Date-Manip perl-TimeDate perl-DBI perl-DBD-Pg perl-Error perl-File-Path perl-List-MoreUtils perl-LDAP perl-Parse-Yapp perl-Sys-Syslog perl-Unicode-String perl-XML-LibXML perl-XML-LibXSLT perl-Text-Iconv perl-Algorithm-Diff perl-Time-Piece perl-Encode-Detect perl-libintl ImageMagick-perl perl-XML-Simple perl-File-Copy-Recursive perl-YAML-Syck perl-Archive-Zip perl-HTML-Tree perl-Unicode-Map8 gcc-c++ xapian-core-devel postgresql-plperl perl-IO-stringy perl-Benchmark-Timer perl-Data-ICal perl-DateTime-Format-ICal perl-HTML-TokeParser-Simple perl-Locale-TextDomain-UTF8 perl-String-Checker perl-Test-WWW-Mechanize perl-Unicode-MapUTF8 perl-XML-Atom-SimpleFeed perl-XML-XML2JSON perl-Search-Xapian perl-ExtUtils-MakeMaker mod_perl p7zip perl-libapreq2 perl-Date-ICal perl-File-MimeInfo perl-HTTP-DAV perl-MIME-tools perl-Term-ReadLine-Gnu perl-Proc-ProcessTable perl-JSON-XS perl-Email-Valid perl-DateTime-Event-ICal rsync perl-LWP-Protocol-https patch

%description
This package contains dependencies for Mioga2.

Project homepage: http://www.alixen.org/projects/mioga2

%install

%files

%changelog
* Fri Mar 17 2017 Alixen Labs. <technique@alixen.fr> - 2.4.26-2
- Added "patch" package

* Mon Aug 29 2016 Alixen Labs. <technique@alixen.fr> - 2.4.26-1
- Initial CentOS-7 meta-package.
